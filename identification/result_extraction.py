import numpy as np
import pandas as pd
from matplotlib_venn import venn3
import matplotlib.pyplot as plt

def compare_id(path_1,path_2,path_3,sample_name):
    df_1 = pd.read_csv(path_1, sep='\t', encoding='latin-1')
    df_2 = pd.read_csv(path_2, sep='\t', encoding='latin-1')
    df_3 = pd.read_csv(path_3, sep='\t', encoding='latin-1')
    peptides_1 = set(df_1['Stripped.Sequence'].tolist())
    protein_1 = set(df_1['Protein.Ids'].tolist())
    peptides_2 = set(df_2['Stripped.Sequence'].tolist())
    protein_2 = set(df_2['Protein.Ids'].tolist())
    peptides_3 = set(df_3['Stripped.Sequence'].tolist())
    protein_3 = set(df_3['Protein.Ids'].tolist())

    venn3((peptides_1, peptides_2, peptides_3), ('custom lib', 'base lib','fine tuned'), set_colors=('g','r','b')) # venn2 works for two sets
    plt.title('Peptide identifications on {} sample'.format(sample_name))
    plt.savefig('venn_diag_pep_{}.png'.format(sample_name))

    plt.clf()
    venn3((protein_1, protein_2, protein_3), ('custom lib', 'base lib','fine tuned'), set_colors=('g','r','b')) # venn2 works for two sets
    plt.title('Protein identifications on {} sample'.format(sample_name))
    plt.savefig('venn_diag_prot_{}.png'.format(sample_name))

def compare_error(path_1,path_2):
    df_1 = pd.read_csv(path_1, sep='\t', encoding='latin-1')
    df_2 = pd.read_csv(path_2, sep='\t', encoding='latin-1')

    peptides_2 = set(df_2['Stripped.Sequence'].tolist())

    df_1=df_1[df_1['Stripped.Sequence'].isin(peptides_2)]

    error_1 = abs(df_1['iRT']-df_1['Predicted.iRT'])
    error_2 = abs(df_2['iRT'] - df_2['Predicted.iRT'])

    plt.hist(error_1)
    plt.savefig('error1.png')
    plt.clf()
    plt.hist(error_2)
    plt.savefig('error2.png')
    return error_1,error_2

def compare_with_db(path):
    df = pd.read_csv(path, sep='\t', encoding='latin-1')
    df_ref = pd.read_excel('250205_All_Peptides_panel_ID_+_RES.xlsx',names=['peptide','fonction'])
    df2=df[df['Stripped.Sequence'].isin(df_ref['peptide'].to_list())]
    corespondance = pd.merge(df2,df_ref,left_on='Stripped.Sequence',right_on='peptide',how="left")

    return corespondance


if __name__ == '__main__':
    # compare_id('CITAMA_ANA_5/julie_custom_nolib.tsv', 'CITAMA_ANA_5/julie_base_nolib.tsv', 'CITAMA_ANA_5/julie_finetune_nolib.tsv','CITAMA_ANA_5_julie_no_lib')
    # e1,e2 = compare_error('CITAMA_ANA_5/report_custom.tsv', 'CITCRO_ANA_3/report_first_lib.tsv')

    cor_base = compare_with_db('CITAMA_ANA_5/julie_base_nolib.tsv')
    cor_custom = compare_with_db('CITAMA_ANA_5/julie_custom_nolib.tsv')
    cor_finetune = compare_with_db('CITAMA_ANA_5/julie_finetune_nolib.tsv')