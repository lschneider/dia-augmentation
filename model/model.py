import math
import torch.nn as nn
import torch

class PermuteLayer(nn.Module):

    def __init__(self, dims):
        super().__init__()
        self.dims = dims

    def forward(self, x):
        x = torch.permute(x, self.dims)
        return x


class PositionalEncoding(nn.Module):

    def __init__(self, d_model: int, dropout: float = 0.1, max_len: int = 26):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)
        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, 1, d_model)
        pe[:, 0, 0::2] = torch.sin(position * div_term)
        pe[:, 0, 1::2] = torch.cos(position * div_term)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = torch.permute(x, (1, 0, 2))
        """
        Arguments:
            x: Tensor, shape ``[seq_len, batch_size, embedding_dim]``
        """
        x = x + self.pe[:x.size(0)]
        return self.dropout(x)


class ModelTransformer(nn.Module):

    def __init__(self, drop_rate=0.1, embedding_dim=128, nb_aa=22, regressor_layer_size_rt=512, decoder_rt_ff=512,
                 n_head=1, seq_length=30, encoder_ff=512, encoder_num_layer=1, decoder_rt_num_layer=1, acti='relu',
                 norm=False):
        self.seq_length = seq_length
        self.nb_aa = nb_aa
        self.n_head = n_head
        self.embedding_dim = embedding_dim
        self.encoder_ff = encoder_ff
        self.encoder_num_layer = encoder_num_layer
        self.decoder_rt_ff = decoder_rt_ff
        self.decoder_rt_num_layer = decoder_rt_num_layer
        self.regressor_layer_size_rt = regressor_layer_size_rt
        self.drop_rate = drop_rate
        super(ModelTransformer, self).__init__()

        self.encoder = nn.TransformerEncoder(nn.TransformerEncoderLayer(d_model=self.embedding_dim, nhead=self.n_head,
                                                                        dim_feedforward=self.encoder_ff,
                                                                        dropout=self.drop_rate, activation=acti,
                                                                        norm_first=norm),
                                             num_layers=self.encoder_num_layer)

        self.decoder_RT = nn.Sequential(
            nn.TransformerEncoder(nn.TransformerEncoderLayer(d_model=self.embedding_dim, nhead=self.n_head,
                                                             dim_feedforward=self.decoder_rt_ff,
                                                             dropout=self.drop_rate, activation=acti, norm_first=norm),
                                  num_layers=self.decoder_rt_num_layer),
            PermuteLayer((1, 0, 2)),
            nn.Flatten(),
            nn.Linear(self.embedding_dim * self.seq_length, self.regressor_layer_size_rt),
            nn.ReLU(),
            nn.Dropout(p=self.drop_rate),
            nn.Linear(self.regressor_layer_size_rt, 1)
        )

        self.emb = nn.Linear(self.nb_aa, self.embedding_dim)

        self.pos_embedding = PositionalEncoding(max_len=self.seq_length, dropout=self.drop_rate,
                                                d_model=self.embedding_dim)

    def forward(self, seq):
        seq_emb = torch.nn.functional.one_hot(seq, self.nb_aa).float()
        emb = self.pos_embedding(self.emb(seq_emb))
        enc = self.encoder(emb)
        out_rt = self.decoder_RT(enc)

        return out_rt.flatten()