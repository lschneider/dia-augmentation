import torch

def distance(x, y):
    return torch.mean(torch.abs(x - y))

