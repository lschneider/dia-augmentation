import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from numpy.ma.core import shape
from sklearn.metrics import r2_score
from constant import ALPHABET_UNMOD, ALPHABET_UNMOD_REV


def length_distribution(data, plot=False, save=False, f_name='fig/data_exploration/length_distribution.png'):
    maximum = 31
    dist = np.zeros(maximum)
    for seq in data:
        dist[len(list(seq)) - seq.count('-') * 2] += 1

    if plot or save:
        plt.stairs(dist, range(maximum + 1), fill=True)
        if plot:
            plt.show()
        if save:
            plt.savefig(f_name)
        plt.clf()
        plt.close()
    return 100 * dist / dist.sum()

def aa_distribution(data, plot=False, save=False, f_name='fig/data_exploration/aa_distribution.png'):
    freq = np.zeros(23)
    for seq in data:
        dec = 0
        for i in range(len(seq)-2*seq.count('-')):
            if seq[dec+i] != '-':
                freq[ALPHABET_UNMOD[seq[dec+i]]] += 1
            elif seq[i+dec+1:i+dec+4] == 'CaC':
                dec += 4
                freq[ALPHABET_UNMOD['CaC']] += 1
            elif seq[i+dec+1:i+dec+4] == 'OxM':
                dec += 4
                freq[ALPHABET_UNMOD['OxM']] += 1

    freq = 100 * freq / freq.sum()

    for i in range(len(freq)) :
        print(freq[i],ALPHABET_UNMOD_REV[i])

    dict_freq = ALPHABET_UNMOD.copy()
    for aa in list(ALPHABET_UNMOD.keys()):
        dict_freq[aa] = freq[ALPHABET_UNMOD[aa]]

    if plot or save:
        plt.bar(list(ALPHABET_UNMOD.keys()), freq, label=list(ALPHABET_UNMOD.keys()))
        if plot:
            plt.show()
        if save:
            plt.savefig(f_name)
        plt.clf()
        plt.close()
    return dict_freq

def numerical_to_alphabetical_str(s):
    seq = ''
    s = s.replace('[','')
    s = s.replace(']', '')
    arr = s.split(',')
    arr = list(map(int, arr))
    for i in range(len(arr)):
        seq+=ALPHABET_UNMOD_REV[arr[i]]
    return seq

def retention_time_distribution(data, plot=False, save=False, f_name='fig/data_exploration/retention_time_distribution.png'):
    plt.hist(data,bins = 50)
    plt.title("Retention Time Distribution")
    if plot:
        plt.show()
    if save:
        plt.savefig(f_name)
    plt.clf()
    plt.close()

def optimal_prediction_r2(df,seq_col,rt_col):
    df_group_1 = df.groupby([seq_col])[rt_col].mean().to_frame().reset_index()
    df_group_1['mean rt']=df_group_1[rt_col]
    df_group_1=df_group_1[[seq_col,'mean rt']]
    df_merged = df_group_1 .merge(df, how='inner', on=seq_col)
    return r2_score(df_merged['mean rt'], df_merged[rt_col])

def main():
    #data prosit
    # df = pd.read_csv('data_prosit/data.csv')
    # _ = length_distribution(df['sequence'],False ,True, '../fig/data_exploration/length_distribution_prosit.png')
    # _ = aa_distribution(df['mod_sequence'], False, True, '../fig/data_exploration/aa_distribution_prosit.png')
    # retention_time_distribution(df['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_prosit.png')
    # df_unique = df[['mod_sequence','irt_scaled']].groupby('mod_sequence').mean()
    # _ = length_distribution(df_unique.index, False, True, '../fig/data_exploration/length_distribution_prosit_unique.png')
    # _ = aa_distribution(df_unique.index, False, True, '../fig/data_exploration/aa_distribution_prosit_unique.png')
    # retention_time_distribution(df_unique['irt_scaled'], False, True,
    #                             '../fig/data_exploration/retention_time_distribution_prosit_unique.png')

    #prosit no cysteine
    # df = pd.read_csv('data_prosit/data_noc.csv')
    # _ = length_distribution(df['sequence'],False ,True, '../fig/data_exploration/length_distribution_prosit_noc.png')
    # _ = aa_distribution(df['mod_sequence'], False, True, '../fig/data_exploration/aa_distribution_prosit_noc.png')
    # retention_time_distribution(df['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_prosit_noc.png')
    # df_unique = df[['mod_sequence','irt_scaled']].groupby('mod_sequence').mean()
    # _ = length_distribution(df_unique.index,False ,True, '../fig/data_exploration/length_distribution_prosit_noc_unique.png')
    # _ = aa_distribution(df_unique.index, False, True, '../fig/data_exploration/aa_distribution_prosit_noc_unique.png')
    # retention_time_distribution(df_unique['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_prosit_noc_unique.png')

    #isa
    # df = pd.read_csv('data_ISA/data_aligned_isa.csv')
    # _ = length_distribution(df['sequence'],False ,True, '../fig/data_exploration/length_distribution_isa.png')
    # _ = aa_distribution(df['sequence'], False, True, '../fig/data_exploration/aa_distribution_isa.png')
    # retention_time_distribution(df['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_isa.png')
    # df_unique = df[['sequence', 'irt_scaled']].groupby('sequence').mean()
    # _ = length_distribution(df_unique.index,False ,True, '../fig/data_exploration/length_distribution_isa_unique.png')
    # _ = aa_distribution(df_unique.index, False, True, '../fig/data_exploration/aa_distribution_isa_unique.png')
    # retention_time_distribution(df_unique['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_isa_unique.png')
    #
    # #isa no cystéine
    # df = pd.read_csv('data_ISA/data_aligned_isa_noc.csv')
    # _ = length_distribution(df['sequence'],False ,True, '../fig/data_exploration/length_distribution_isa_noc.png')
    # _ = aa_distribution(df['sequence'], False, True, '../fig/data_exploration/aa_distribution_isa_noc.png')
    # retention_time_distribution(df['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_isa_noc.png')
    # df_unique = df[['sequence', 'irt_scaled']].groupby('sequence').mean()
    # _ = length_distribution(df_unique.index,False ,True, '../fig/data_exploration/length_distribution_isa_noc_unique.png')
    # _ = aa_distribution(df_unique.index, False, True, '../fig/data_exploration/aa_distribution_isa_noc_unique.png')
    # retention_time_distribution(df_unique['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_isa_noc_unique.png')

    #isa mox
    # df = pd.read_csv('data_ISA_mox/data_aligned_isa_noc.csv')
    # _ = length_distribution(df['sequence'],False ,True, '../fig/data_exploration/length_distribution_isa_noc_mox.png')
    # _ = aa_distribution(df['sequence'], False, True, '../fig/data_exploration/aa_distribution_isa_noc_mox.png')
    # retention_time_distribution(df['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_isa_noc_mox.png')
    # df_unique = df[['sequence', 'irt_scaled']].groupby('sequence').mean()
    # _ = length_distribution(df_unique.index,False ,True, '../fig/data_exploration/length_distribution_isa_noc_mox_unique.png')
    # _ = aa_distribution(df_unique.index, False, True, '../fig/data_exploration/aa_distribution_isa_noc_mox_unique.png')
    # retention_time_distribution(df_unique['irt_scaled'], False, True, '../fig/data_exploration/retention_time_distribution_isa_noc_mox_unique.png')

    #prosit outlier_plasma
    df = pd.read_csv('data_ISA/data_prosit_outlier.csv')
    df['seq']=df['seq'].map(numerical_to_alphabetical_str)
    _ = length_distribution(df['seq'],False ,True, '../fig/data_exploration/length_distribution_ISA_prosit_outlier.png')
    _ = aa_distribution(df['seq'], False, True, '../fig/data_exploration/aa_distribution_ISA_prosit_outlier.png')
    retention_time_distribution(df['true rt'], False, True, '../fig/data_exploration/retention_time_distribution_ISA_prosit_outlier.png')

    #compare variance of outliers vs others petides
    seq_out_list = df['seq'].to_list()
    df_prosit = pd.read_csv('data_prosit/data_noc.csv')

if __name__ == '__main__':
    df = pd.read_csv('data_PXD006109/plasma/data_prosit_outlier.csv')
    df['seq'] = df['seq'].map(numerical_to_alphabetical_str)

    # compare variance of outliers vs others petides
    seq_out_list = df['seq'].to_list()
    df_prosit = pd.read_csv('data_prosit/data.csv')
    df_prosit['outlier']=df_prosit['sequence'].map(lambda x : x in seq_out_list)
    df_prosit_outlier = df_prosit[df_prosit['outlier']==True]
    df_agg_out = df_prosit_outlier.groupby(pd.Grouper(key='mod_sequence'))['irt_scaled'].agg(['mean', 'median', 'var']).reset_index()
    df_agg_pro = df_prosit.groupby(pd.Grouper(key='mod_sequence'))['irt_scaled'].agg(['mean', 'median', 'var']).reset_index()
    # main()

    plt.hist(df_agg_out['var'])
    plt.savefig('var_outlier.png')
    plt.hist(df_agg_pro['var'])
    plt.savefig('var_pro.png')