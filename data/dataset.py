import torch
from torch.utils.data import Dataset, DataLoader
import pandas as pd

ALPHABET_UNMOD = {
    "_": 0,
    "A": 1,
    "C": 2,
    "D": 3,
    "E": 4,
    "F": 5,
    "G": 6,
    "H": 7,
    "I": 8,
    "K": 9,
    "L": 10,
    "M": 11,
    "N": 12,
    "P": 13,
    "Q": 14,
    "R": 15,
    "S": 16,
    "T": 17,
    "V": 18,
    "W": 19,
    "Y": 20,
    "CaC": 22,
    "OxM": 21,
}


def padding(arr,length):
    if len(arr)>=length:
        return arr
    else :
        return arr +[0]*(length - len(arr))



def alphabetical_to_numerical(seq):
    num = []
    dec = 0
    for i in range(len(seq) - 2 * seq.count('-')):
        if seq[i + dec] != '-':
            num.append(ALPHABET_UNMOD[seq[i + dec]])
        else:
            if seq[i + dec + 1:i + dec + 4] == 'CaC':
                num.append(ALPHABET_UNMOD['CaC'])
            elif seq[i + dec + 1:i + dec + 4] == 'OxM':
                num.append(ALPHABET_UNMOD['OxM'])
            else:
                raise 'Modification not supported'
            dec += 4
    return num


class RT_Dataset(Dataset):

    def __init__(self, size, data_source, mode, length, format='iRT_scaled', seq_col='sequence'):
        print('Data loader Initialisation')
        self.data = pd.read_csv(data_source)

        self.mode = mode
        self.format = format

        print('Selecting data')
        if mode == 'train':
            self.data = self.data[self.data.state == 'train']
        elif mode == 'test':
            self.data = self.data[self.data.state == 'holdout']
        elif mode == 'validation':
            self.data = self.data[self.data.state == 'validation']
        if size is not None:
            self.data = self.data.sample(size)



        print('Converting')
        self.data['sequence'] = self.data['sequence'].map(alphabetical_to_numerical)

        print('Padding')
        self.data['sequence'] = self.data['sequence'].map(lambda x : padding(x,length))
        self.data = self.data.drop(self.data[self.data['sequence'].map(len) > length].index)

        self.data = self.data.reset_index()

    def __getitem__(self, index: int):
        seq = self.data['sequence'][index]
        if self.format == 'RT':
            label = self.data['retention_time'][index]
        if self.format == 'iRT':
            label = self.data['irt'][index]
        if self.format == 'iRT_scaled':
            label = self.data['irt_scaled'][index]
        return torch.tensor(seq), torch.tensor(label).float()

    def __len__(self) -> int:
        return self.data.shape[0]


def load_data(batch_size, data_source, length=25, mode='train', size=None, seq_col = 'sequence'):
    print('Loading data')
    data = RT_Dataset(size = None, data_source=data_source, mode=mode, length=length, seq_col=seq_col)
    data_loader = DataLoader(data, batch_size=batch_size, shuffle=True)

    return data_loader